[
  line_length: 80,
  import_deps: [:phoenix],
  inputs: ["*.{ex,exs}", "{config,lib,briv,test}/**/*.{ex,exs}"]
]
