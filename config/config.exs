import Config

# Configures the endpoint
config :note_blog, NoteBlogWeb.Endpoint,
  url: [host: "localhost"],
  http: [ip: {127, 0, 0, 1}],
  secret_key_base:
    "Qv/YDAQEOqXkhMsWNXjD5U4140txBdcF0oh+6NIkz60/zKP9v2cmUq884uSnNpoV",
  render_errors: [view: NewWeb.ErrorView, accepts: ~w(html json), layout: false],
  pubsub_server: NoteBlog.PubSub,
  live_view: [signing_salt: "l3f7ShB8"]

# Configure esbuild (the version is required)
config :esbuild,
  version: "0.14.29",
  default: [
    args:
      ~w(js/app.js --bundle --target=es2017 --outdir=../priv/static/assets --external:/fonts/* --external:/images/*),
    cd: Path.expand("../assets", __DIR__),
    env: %{"NODE_PATH" => Path.expand("../deps", __DIR__)}
  ]

# Configures Elixir's Logger
config :logger, :console,
  format: "$time $metadata[$level] $message\n",
  metadata: [:user_id]

config :phoenix, :template_engines, md: PhoenixPandoc.Engine

config :phoenix, :json_library, Jason

config :note_blog, root: NoteBlog.Template.PostRoot

# Import environment specific config. This must remain at the bottom
# of this file so it overrides the configuration defined above.
import_config "#{config_env()}.exs"
