defmodule NoteBlogWeb.Router do
  use NoteBlogWeb, :router

  pipeline :browser do
    plug(:accepts, ["html"])
    plug(:fetch_session)
    plug(:fetch_flash)
    plug(:protect_from_forgery)
    plug(:put_secure_browser_headers)
  end

  pipeline :api do
    plug(:accepts, ["json"])
  end

  scope "/", NoteBlogWeb do
    # Use the default browser stack
    pipe_through(:browser)

    get("/post", PostController, :index)
    get("/post/:uri", PostController, :index)
    get("/post/*uri", PostController, :show)

    get("/tag", TagController, :index)
    get("/tag/*tag", TagController, :show)
  end

  # Other scopes may use custom stacks.
  # scope "/api", NoteBlogWeb do
  #   pipe_through :api
  # end
end
