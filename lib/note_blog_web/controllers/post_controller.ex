defmodule NoteBlogWeb.PostController do
  use NoteBlogWeb, :controller

  alias NoteBlog.Template
  alias NoteBlogWeb.PostView

  def index(%Plug.Conn{request_path: "/post" <> uri} = conn, _params) do
    uri = String.trim(uri, "/")

    assigns =
      uri
      |> PostView.assigns()
      |> Map.put(:posts, Template.list_posts(uri))

    conn
    |> put_layout("topic.html")
    |> render("index.html", assigns)
  end

  def show(%Plug.Conn{request_path: "/post/" <> uri} = conn, _params) do
    assigns = uri |> URI.decode() |> PostView.assigns()

    conn
    |> put_layout("topic.html")
    |> render("show.html", assigns)
  end
end
