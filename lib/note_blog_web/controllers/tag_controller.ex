defmodule NoteBlogWeb.TagController do
  use NoteBlogWeb, :controller

  alias NoteBlogWeb.TagView

  def index(conn, _params) do
    conn
    |> put_layout("topic.html")
    |> render("index.html", TagView.assigns())
  end

  def show(%Plug.Conn{request_path: "/tag/" <> tag} = conn, _params) do
    assigns = tag |> URI.decode() |> TagView.assigns()

    if assigns.posts == [] do
      put_status(conn, 404)
    else
      conn
      |> put_layout("topic.html")
      |> render("show.html", assigns)
    end
  end
end
