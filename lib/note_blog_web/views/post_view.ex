defmodule NoteBlogWeb.PostView do
  use NoteBlogWeb, :view

  alias NoteBlog.Post
  alias NoteBlog.Template

  import NoteBlogWeb.PostView.HTML
  import NoteBlogWeb.PostView.Markdown
  import NoteBlogWeb.PostView.Time

  def render("show.html" = template, assigns) do
    {:safe, contents} = render_template(template, assigns)

    {:safe, [table_of_contents(contents) | ["<hr>\n" | contents]]}
  end

  @doc "Render the `NoteBlog.Post` struct given a uri"
  def render_post(uri) do
    uri
    |> Template.post_json_path()
    |> render()
    |> Jason.decode!(keys: :atoms)
    |> Map.merge(%{uri: uri})
    |> (&Kernel.struct(Post, &1)).()
  end

  @doc ~s"""
  Render a template stored inside a post.

  Convenience function to make it easier to split posts into multiple files
  for organization.
  """
  def render_inside_post(uri, relative_path, assigns) do
    template = Path.join(uri, relative_path)
    render_to_iodata(__MODULE__, template, assigns)
  end

  @doc ~s"""
  Get `assigns` map for the given URI

  Currently, it is the `NoteBlog.Post` for that post, converted into a map.
  """
  def assigns(uri) do
    uri
    |> render_post()
    |> Map.from_struct()
  end
end
