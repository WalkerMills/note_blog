defmodule NoteBlogWeb.PostView.HTML do
  alias NoteBlog.Template

  def table_of_contents(contents) do
    contents
    |> IO.chardata_to_string()
    |> Floki.find("h1, h2, h3, h4, h5, h6")
    |> headers_to_toc()
  end

  defp headers_to_toc([]), do: []
  defp headers_to_toc(nodes), do: nodes |> do_headers_to_toc("", 0) |> elem(1)

  defp do_headers_to_toc([], output, _level), do: {[], output}

  defp do_headers_to_toc([{h, attr, [text]} | tail] = nodes, output, level) do
    new_level = header_to_level(h)

    cond do
      new_level > level ->
        {nodes, nested} = do_headers_to_toc(nodes, "", new_level)
        output = [output, "<ul>\n", nested, "</ul>\n"]
        do_headers_to_toc(nodes, output, level)

      new_level < level ->
        {nodes, output}

      new_level == level ->
        entry = toc_entry(attr, text)
        do_headers_to_toc(tail, [output | entry], level)
    end
  end

  defp header_to_level(h) do
    ~r/h([1-6])/
    |> Regex.run(h, capture: :all_but_first)
    |> List.first()
    |> String.to_integer()
  end

  defp toc_entry(attributes, text) do
    id = id_from_attributes(attributes)
    ["<li><a href=\"\#", id, "\">", text, "</a></li>\n"]
  end

  defp id_from_attributes([]), do: ""
  defp id_from_attributes([{"id", id} | _tail]), do: id
  defp id_from_attributes([_head | tail]), do: id_from_attributes(tail)

  defp upload_once!(data_uri) do
    upload_path = Template.to_upload_path(data_uri)

    if not File.exists?(upload_path) do
      :ok = upload_path |> Path.dirname() |> File.mkdir_p!()
      :ok = data_uri |> Template.to_template_path() |> File.ln!(upload_path)
    end

    Template.to_serving_path(data_uri)
  end

  def pdf_object!(uri, relative_path, width, height) do
    data_uri = Path.join(uri, relative_path)
    serving_path = upload_once!(data_uri)

    [
      "<object type=\"application/pdf\" width=\"",
      width,
      "\" height=\"",
      height,
      "\" ",
      "data=\"",
      serving_path,
      "\"><p>Error: data not found at ",
      serving_path,
      "</p></object>"
    ]
  end

  def img!(uri, relative_path, alt, width, height) do
    data_uri = Path.join(uri, relative_path)
    serving_path = upload_once!(data_uri)

    [
      "<img src=\"",
      serving_path,
      "\" alt=\"",
      alt,
      "\" width=\"",
      width,
      "\" height=\"",
      height,
      "\">"
    ]
  end
end
