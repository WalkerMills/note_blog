defmodule NoteBlogWeb.PostView.Markdown do
  alias NoteBlog.Template

  @doc "Convert a post's URI into a `Plug.Conn.request_path`"
  def link_to(uri) do
    "/post/#{uri}"
  end

  defp header_rows(:ok), do: ""
  defp header_rows({:error, _}), do: ""
  defp header_rows({:ok, ""}), do: ""
  defp header_rows([item = {:ok, _}]), do: header_rows(item)

  defp header_rows(item = {:ok, row}) do
    [table_row(item), "|", Enum.map(row, fn _ -> ":-:|" end), "\n"]
  end

  defp table_row(:ok), do: ""
  defp table_row({:error, _}), do: ""
  defp table_row({:ok, ""}), do: ""

  defp table_row({:ok, row}) do
    ["|", Enum.map(row, fn item -> [item, "|"] end), "\n"]
  end

  @doc "Render a CSV file into a Markdown table"
  def csv_table!(uri, relative_path, <<separator::utf8>>) do
    stream =
      uri
      |> Template.to_template_path()
      |> Path.join(relative_path)
      |> File.stream!()
      |> CSV.decode(separator: separator, strip_fields: true)

    header = stream |> Enum.take(1) |> header_rows()
    body = stream |> Enum.drop(1) |> Enum.map(&table_row/1)

    [header, body]
  end
end
