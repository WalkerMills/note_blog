defmodule NoteBlogWeb.PostView.Time do
  @doc "Convert a serialized DateTime to a serialized Date"
  def datetime_to_date(datetime) do
    datetime
    |> DateTime.from_iso8601()
    |> elem(1)
    |> DateTime.to_date()
    |> to_string()
  end
end
