defmodule NoteBlogWeb.TagView do
  use NoteBlogWeb, :view

  alias NoteBlog.Tags

  def assigns do
    {:ok, tags} = Tags.count()

    %{
      title: "Index of Tags",
      created: DateTime.utc_now() |> DateTime.to_string(),
      tags: tags
    }
  end

  def assigns(tag) do
    {:ok, uris} = Tags.tagged(tag)

    %{
      title: title(tag),
      created: DateTime.utc_now() |> DateTime.to_string(),
      posts: uris
    }
  end

  def title(tag), do: "\##{tag}"

  def link_to(tag), do: "/tag/#{tag}"
end
