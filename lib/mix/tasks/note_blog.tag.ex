defmodule Mix.Tasks.NoteBlog.Tag do
  @shortdoc ~s"Add tags to a post."
  @moduledoc ~s"""
  #{@shortdoc}

  ## Command line arguments

  * `uri` -- Path of the post to tag relative to the template root.
  * `tags` -- A space-separated list of tags to add to the post.

  See `NoteBlog.Template.Gen.add_tags!/2` for more details on valid
  arguments.
  """

  use Mix.Task

  alias NoteBlog.Template.Gen

  @doc false
  def run([uri]) do
    Mix.raise("No tags supplied for #{uri}")
  end

  @switches [test: :boolean]

  @aliases [t: :test]

  @doc false
  def run(args) do
    {options, [uri | tags], []} =
      OptionParser.parse(args, aliases: @aliases, switches: @switches)

    Gen.add_tags!(uri, tags, options)
  end
end
