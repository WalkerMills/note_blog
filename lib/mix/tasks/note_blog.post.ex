defmodule Mix.Tasks.NoteBlog.Post do
  @shortdoc "Generate a new post (skeleton)."
  @moduledoc ~s"""
  #{@shortdoc}

  ## Command line arguments

  * `uri` - Path for the new post relative to the post template root
  * `title` - Title for the new post.
  * `tags` - Space separated list of tags for the new post (optional)
  * `--force`, `-f` - Overwrite any existing post.
  """

  use Mix.Task

  alias NoteBlog.Template.Gen

  @switches [force: :boolean, test: :boolean]

  @aliases [f: :force, t: :test]

  @doc false
  def run(args) do
    {uri, options} =
      args
      |> OptionParser.parse(aliases: @aliases, switches: @switches)
      |> parse_arguments()

    Gen.initialize_post!(uri, options)
  end

  defp parse_arguments({options, args, []}) do
    {uri, more_options} = parse_arguments(args)
    {uri, options ++ more_options}
  end

  defp parse_arguments([]), do: {"", []}

  defp parse_arguments([uri]), do: {uri, []}

  defp parse_arguments([uri, title | tags]) do
    {uri, [title: title, tags: tags]}
  end
end
