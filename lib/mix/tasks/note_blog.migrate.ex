defmodule Mix.Tasks.NoteBlog.Migrate do
  @shortdoc "Migrate post templates to new file layout."
  @moduledoc ~s"""
  #{@shortdoc}

  ## Command line arguments

  * `-r`/`--remove` - Remove old files after migrating
  * `-c`/`--clean` - Remove new files; does not perform a migration and
    supersedes `--remove` flag
  """

  use Mix.Task

  alias NoteBlog.Template

  @switches [remove: :boolean, clean: :boolean]

  @aliases [r: :remove, c: :clean]

  @root_regex ~r"(#{Template.root()}/?(private/?)?)(.*)"

  @doc false
  def run(args) do
    {options, _, []} =
      OptionParser.parse(args, aliases: @aliases, switches: @switches)

    dirs =
      Template.root()
      |> Path.join("**/post.json.eex")
      |> Path.wildcard()
      |> Enum.map(&Path.dirname/1)
      |> Enum.map(&compute_paths/1)

    if options[:clean] do
      :ok = remove!(dirs, :clean)
    else
      :ok = migrate!(dirs)

      if options[:remove] do
        :ok = remove!(dirs)
      end
    end
  end

  def compute_paths(path) do
    [_, root, _, uri] = Regex.run(@root_regex, path)

    new_uri = Path.basename(uri)
    {Path.join(root, uri), Path.join(root, new_uri)}
  end

  def remove!([], _), do: :ok
  def remove!([{uri, uri} | tail], :clean = tag), do: remove!(tail, tag)

  def remove!([{_, new_uri} | tail], :clean = tag) do
    File.rm_rf!(new_uri)
    remove!(tail, tag)
  end

  def remove!([]), do: :ok
  def remove!([{uri, uri} | tail]), do: remove!(tail)

  def remove!([{old_uri, _} | tail]) do
    File.rm_rf!(old_uri)
    remove!(tail)
  end

  def migrate!([]), do: :ok
  def migrate!([{uri, uri} | tail]), do: migrate!(tail)

  def migrate!([{old_uri, new_uri} | tail]) do
    File.cp_r!(old_uri, new_uri)
    migrate!(tail)
  end
end
