defmodule NoteBlog do
  @moduledoc """
  This module defines the API for working with content.

  ### API Notes

  -   Posts live in `lib/note_blog_web/templates/post`

  -   Each post is a directory, but not every directory is a post. Directories
      may be nested arbitrarily.

  -   Posts are directories which contain two files: `show.json` for metadata,
      and `show.html` for content. These can be EEx or Markdown templates

  -   Template files ending in `.md` are interpreted as Markdown. For example,
      a file ending in `.html.md` is a Markdown file that renders to HTML.
  """
end
