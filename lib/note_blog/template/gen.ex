defmodule NoteBlog.Template.Gen do
  @moduledoc "Generate new templates and/or content"

  @test_root "test/post"

  alias NoteBlog.Template

  defp valid_path!(uri, test?) do
    path = to_template_path(uri, test?)

    unless Template.initialized?(path) do
      raise(ArgumentError, path <> " does not contain a valid post")
    end

    path
  end

  defp to_template_path(uri, test?) do
    if test? do
      Path.join(@test_root, uri)
    else
      Template.to_template_path(uri)
    end
  end

  @doc "Add tags to an existing post."
  @spec add_tags!(String.t(), [String.t()], keyword()) :: :ok | no_return()
  def add_tags!(uri, tags, options \\ []) do
    test? = Keyword.get(options, :test, false)
    path = uri |> valid_path!(test?) |> Path.join(Template.post_template())

    post =
      path
      |> File.read!()
      |> Jason.decode!(keys: :atoms)

    post
    |> Map.get(:tags, [])
    |> Enum.concat(tags)
    |> Enum.uniq()
    |> (&Map.put(post, :tags, &1)).()
    |> Jason.encode!()
    |> (&File.write!(path, &1, [:write])).()
  end

  @doc ~s"""
  Initialize a post at the given URI (generate templates).

  This function generates all the templates necessary for a post to render. By
  default this will not overwrite any existing posts. Set the `force` option to
  `true` to change this behavior. 

  ## Options

  Supported options are:

  * :title - The title of the new post.
  * :tags - A list of tags for the post.
  * :force - Whether or not to overwrite existing posts.
  * :test - For testing purposes only.
  """
  @spec initialize_post!(String.t(), keyword()) :: :ok | no_return()
  def initialize_post!(uri, options \\ []) do
    force? = Keyword.get(options, :force, false)
    test? = Keyword.get(options, :test, false)

    uri = String.trim(uri, "/")
    path = to_template_path(uri, test?)

    cond do
      uri |> Path.split() |> Enum.count() > 2 ->
        raise(
          ArgumentError,
          "'#{uri}' has more than two path components"
        )

      !File.exists?(path) ->
        File.mkdir_p!(path)

      !force? and Template.initialized?(path) ->
        raise(ArgumentError, "There is already a post at " <> path)

      true ->
        :ok
    end

    namespace = uri |> Path.split() |> List.first()
    namespace = if namespace == nil, do: "", else: namespace

    initialize_namespace(namespace, force?, test?)

    if namespace != uri do
      title = Keyword.get(options, :title, "")
      tags = Keyword.get(options, :tags, [])

      generate_post(path, title, tags)
      generate_show(path)
    end
  end

  defp initialize_namespace(namespace, force?, test?) do
    path = to_template_path(namespace, test?)

    if !Template.initialized?(path) || force? do
      generate_post(path, title_of(namespace))
      generate_show(path)
    end
  end

  defp title_of("") do
    Template.root()
    |> Path.split()
    |> List.last()
    |> (&("Index of " <> &1)).()
  end

  defp title_of(namespace), do: "Index of #{namespace}"

  defp generate_post(path, title, tags \\ []) do
    post =
      __DIR__
      |> Path.join("post.json.eex")
      |> EEx.eval_file(title: title)
      |> Jason.decode!()
      |> Map.put(:tags, tags)
      |> Jason.encode!()

    path |> Path.join(Template.post_template()) |> File.write!(post, [:write])
  end

  defp generate_show(path) do
    path |> Path.join(Template.show_template()) |> File.touch!()
  end
end
