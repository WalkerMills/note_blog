defmodule NoteBlog.Post do
  @moduledoc ~s"""
  Metadata for a post.

  ## Fields

  * `:uri` -- The path of the post relative to the template root.
  * `:title` -- The title of the post.
  * `:created` -- The UTC timestamp of the post's creation time.
  * `:tags` -- A list of tags
  """

  use TypedStruct

  typedstruct enforce: true do
    field(:uri, String.t())
    field(:title, String.t())
    field(:created, String.t())
    field(:tags, [String.t()], default: [])
  end
end
