defmodule NoteBlog.Template.Root do
  @moduledoc false
  @callback path :: String.t()

  defmacro __using__(options) do
    path = Keyword.get(options, :path)

    quote do
      @behaviour unquote(__MODULE__)

      @impl true
      def path, do: unquote(path)
    end
  end
end

defmodule NoteBlog.Template.PostRoot do
  @moduledoc false
  use NoteBlog.Template.Root, path: "lib/note_blog_web/templates/post"
end

defmodule NoteBlog.Template do
  @moduledoc ~s"""
  This module contains the logic for managing templates.
  """

  @index_template "index.html.eex"
  @post_template "post.json.eex"
  @show_template "show.html.md"

  @root Application.get_env(:note_blog, :root)

  @just_uri ~r".*?#{@root.path()}/?(.*?)(/(post\.json(\.eex)?|show\.html(\.md|\.eex)?))?$"

  @doc "Path to the post template root."
  def root, do: @root.path()

  @doc "The filename of the template that indexes posts"
  def index_template, do: @index_template

  @doc "The filename of the template that contains the `NoteBlog.Post` struct"
  def post_template, do: @post_template

  @doc "The filename of the template that contains the post body"
  def show_template, do: @show_template

  @doc "Convert a post (URI) to a path relative to the template root."
  def to_template_path(uri), do: root() |> Path.join(uri)

  @doc "Convert a path including the template root to a post URI."
  def to_uri(path) do
    path
    |> (&Regex.run(@just_uri, &1, capture: :all_but_first)).()
    |> List.first()
  end

  @doc "Check whether the given path contains an initialized post."
  def initialized?(path) do
    Enum.all?(
      ["post.json*", "show.html*"],
      &wildcard_exists?(path, &1)
    )
  end

  defp wildcard_exists?(path, pattern) do
    !(path |> Path.join(pattern) |> Path.wildcard() |> Enum.empty?())
  end

  @doc "List all posts"
  def list_posts do
    [root() | ls_directories(root())]
    |> do_list_posts()
  end

  @doc "List all posts inside the given namespace."
  def list_posts(""), do: list_posts()

  def list_posts(namespace) do
    namespace
    |> to_template_path()
    |> List.wrap()
    |> do_list_posts()
  end

  defp do_list_posts(dirs) do
    dirs
    |> Enum.map(&ls_directories/1)
    |> List.flatten()
    |> Enum.filter(&initialized?/1)
    |> Enum.map(&to_uri/1)
  end

  defp ls_directories(path) do
    path
    |> File.ls()
    |> case do
      {:error, _} -> []
      {:ok, output} -> output
    end
    |> Enum.filter(&(!String.starts_with?(&1, ".")))
    |> Enum.map(&Path.join(path, &1))
    |> Enum.filter(&File.dir?/1)
  end

  def post_json_path(uri), do: Path.join(uri, "post.json")

  @serving_prefix "/uploads"
  def serving_prefix(), do: @serving_prefix

  @upload_root Application.app_dir(
                 :note_blog,
                 Path.join("priv", @serving_prefix)
               )
  def upload_root(), do: @upload_root

  def to_upload_path(uri), do: Path.join(upload_root(), uri)

  def to_serving_path(uri), do: Path.join(serving_prefix(), uri)
end
