defmodule NoteBlog.Tags do
  @moduledoc ~s"""
  Map URI's to tags, and tags to URI's.

  This module implements a `GenServer` so that it can crawl the post templates
  once and store the bidirectional mapping in memory. It also supports live
  reloading using the `FileSystem` module. By default it is off, and enabled in
  dev mode by setting the `:reload_tags` config option to `true`.
  """

  use GenServer

  alias NoteBlog.Template

  # Client API

  @doc "Start the tag server"
  def start_link do
    GenServer.start_link(__MODULE__, :ok, name: __MODULE__)
  end

  @doc "Start the tag server with options"
  def start_link(opts) do
    GenServer.start_link(__MODULE__, :ok, opts)
  end

  @doc "Returns a list of tuples of {occurrences, tag}."
  def count do
    GenServer.call(__MODULE__, {:count})
  end

  @doc ~s"""
  Look up the tags for a given URI.

  Returns `{:ok, [tag]}` if the URI is valid, and `:error` otherwise.
  """
  def tags(uri) do
    GenServer.call(__MODULE__, {:tags, uri})
  end

  @doc ~s"""
  Look up the URI's containing the given tag

  Returns `{:ok, [uri]}` if the tag is valid, and `:error` otherwise.
  """
  def tagged(tag) do
    GenServer.call(__MODULE__, {:tagged, tag})
  end

  # Server callbacks

  @doc ~s"""
  Initialize the tag mappings.

  Crawls the template root and reads the tags for each post into memory.
  """
  @impl true
  def init(:ok) do
    reload? = Application.get_env(:note_blog, :reload_tags, false)

    if reload? do
      {:ok, pid} = FileSystem.start_link(dirs: [Template.root()])
      FileSystem.subscribe(pid)
    end

    tag_map =
      Template.list_posts()
      |> Enum.map(&uri_and_tags/1)
      |> List.flatten()
      |> BiMultiMap.new()

    {:ok, tag_map}
  end

  defp uri_and_tags(uri) do
    for tag <- read_tags(uri), do: {uri, tag}
  end

  defp read_tags(uri) do
    uri
    |> Template.to_template_path()
    |> Path.join(Template.post_template())
    |> File.read!()
    |> Jason.decode!(keys: :atoms)
    |> Map.get(:tags, [])
  end

  @impl true
  def handle_call({:count}, _from, tag_map) do
    counts =
      tag_map
      |> BiMultiMap.right()
      |> Enum.map(fn {tag, uris} -> {Enum.count(uris), tag} end)

    {:reply, {:ok, counts}, tag_map}
  end

  @impl true
  def handle_call({:tags, uri}, _from, tag_map) do
    {:reply, {:ok, BiMultiMap.get(tag_map, uri, [])}, tag_map}
  end

  @impl true
  def handle_call({:tagged, tag}, _from, tag_map) do
    {:reply, {:ok, BiMultiMap.get_keys(tag_map, tag, [])}, tag_map}
  end

  # Live reloading (handle FileSystem messages)

  @impl true
  def handle_info({:file_event, _pid, {path, events}}, tag_map) do
    if :deleted in events or update?(path) do
      {:noreply, update_tags(tag_map, path, events), {:continue, :reload}}
    else
      {:noreply, tag_map}
    end
  end

  defp update?(path) do
    Regex.match?(~r/.*#{Template.post_template()}/, path) or
      Template.initialized?(path)
  end

  defp update_tags(%BiMultiMap{} = tag_map, path, events) do
    uri = Template.to_uri(path)

    if :deleted in events do
      BiMultiMap.delete_key(tag_map, uri)
    else
      tag_map |> BiMultiMap.delete_key(uri) |> tag_uri(uri)
    end
  end

  defp tag_uri(%BiMultiMap{} = tag_map, uri) do
    uri
    |> read_tags()
    |> List.foldl(tag_map, fn tag, tag_map ->
      BiMultiMap.put(tag_map, uri, tag)
    end)
  end

  @impl true
  def handle_continue(:reload, tag_map) do
    # Trigger Phoenix.LiveReloader for the tag templates
    File.touch!("lib/note_blog_web/templates/tag/index.html.eex")
    File.touch!("lib/note_blog_web/templates/tag/show.html.eex")

    {:noreply, tag_map}
  end
end
