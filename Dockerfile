FROM debian:buster
LABEL maintainer="walker.mills7@gmail.com"

# Basic sanity
ARG DEBIAN_FRONTEND=noninteractive
RUN apt-get update
RUN apt-get install dialog apt-utils -y

# Set the locale
RUN apt-get install locales -y
ENV LANG="en_US.UTF-8"
ENV LANGUAGE="en_US.UTF-8"
ENV LC_ALL="en_US.UTF-8"
RUN sed -i -e "s/# $LANG/$LANG/" /etc/locale.gen
RUN locale-gen
RUN dpkg-reconfigure --frontend=noninteractive locales
RUN update-locale LANG="$LANG"

# Install Elixir + Phoenix
RUN apt-get install erlang elixir inotify-tools -y
RUN mix local.hex --force
RUN mix local.rebar --force
RUN mix archive.install https://github.com/phoenixframework/archives/raw/master/phx_new.ez --force

# Start the Phoenix application
ARG PHOENIX_ROOT="/phoenix_app"
EXPOSE 4000
COPY phoenix_blog $PHOENIX_ROOT
WORKDIR $PHOENIX_ROOT 
RUN mix deps.get --force
RUN mix deps.compile --force
