defmodule PostViewTest do
  use ExUnit.Case, async: true

  alias NoteBlogWeb.PostView

  test "static template, no toc" do
    assert "test/post/static.html"
           |> File.read!()
           |> PostView.table_of_contents() == []
  end

  test "table of contents" do
    assert "test/post/toc.html.eex"
           |> File.read!()
           |> PostView.table_of_contents()
           |> IO.chardata_to_string()
           |> Floki.parse() ==
             [
               {"ul", [],
                [
                  {"li", [], [{"a", [{"href", "#foo"}], ["a header"]}]},
                  {"ul", [],
                   [
                     {"li", [],
                      [{"a", [{"href", "#bar"}], ["a nested header"]}]},
                     {"ul", [],
                      [
                        {"li", [],
                         [{"a", [{"href", "#baz"}], ["another nested header"]}]},
                        {"li", [],
                         [{"a", [{"href", "#foobar"}], ["a sibling"]}]}
                      ]},
                     {"li", [], [{"a", [{"href", "#qux"}], ["up a level"]}]},
                     {"ul", [],
                      [
                        {"li", [],
                         [{"a", [{"href", "#quz"}], ["skip a level down"]}]}
                      ]}
                   ]},
                  {"li", [], [{"a", [{"href", "#quux"}], ["skip a level up"]}]}
                ]},
               {"ul", [],
                [
                  {"li", [],
                   [{"a", [{"href", "#quuz"}], ["a higher-level header"]}]}
                ]}
             ]
  end
end
