defmodule TestTemplateRoot do
  use NoteBlog.Template.Root, path: "test/post"
end

Application.put_env(:note_blog, :root, TestTemplateRoot)

ExUnit.start()
