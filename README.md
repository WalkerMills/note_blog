# Noteblog

A personal knowledge management system, written in Elixir using the Phoenix web
framework. It is a content management system similar in spirit to a wiki, but
with a focus on personal organization rather than collaboration.

## Dependencies

This project uses Elm for its frontend, and `npm` for managing Javascript
dependencies. The Elm compiler must be available on your system for everything
to build correctly. You can follow the instructions
[here](https://www.npmjs.com/get-npm) and
[here](https://guide.elm-lang.org/install.html), or if you are on Arch Linux,
you can install everything like so:

```shell
pacaur -S npm elm-platform-bin
```

To initialize Javascript dependencies:

```shell
cd assets
npm install
cd ../
```

## Usage

To start your Phoenix server locally:

```shell
mix deps.get
mix phx.server
```

Now you can visit [`localhost:4000/post`](http://localhost:4000/post) from your
browser to view your content.

To read the documentation locally, first generate it with `mix docs`. If your
Phoenix server is running, you can then visit
[`localhost:4000/docs/index.html`](http://localhost:4000/docs/index.html) to
view it.  Otherwise, you can manually open `priv/static/docs/index.html` in
your browser.

### Creating Content

Noteblog includes template generators to make it easier to create new content.
These code generators are implemented as `mix` tasks. Their source code and
templates can be found in `lib/mix/tasks`

## Design

The knowledge and experience I collect are the foundations of my creativity,
whether I am building on them or picking them apart. I need them to be
accessible, expressive, and easy to share. I normally use a notebook, but pen
& paper is at most one of those. Blogs are great for publishing, but they
lack a lot of the features of good note-taking software. For example,
privacy. Most of the raw content I produce doesn't make for very good reading
after all.

Hypertext has all the features I want from my note system (links, markup,
etc.), allows me to leverage browsers as a cross-platform frontend, and neatly
solves the issue of publication. A static site generator (not trying to write
HTML by hand) would work at first, but I plan add interactive features to allow
me to better leverage my knowledge. Elixir's precompiled templates and
immutable variables offer a highly performant, dynamic backend.

### What Does It Do?

-   It's a flat-file CMS, combining version-controlled content with
  dynamic functionality.
-   Supports EEx and Markdown templates, and live preview when running
  locally.
-   Topics and tags. Topics can be nested, and a post can belong to
  multiple topics.
-   Chronological index of posts by topic
-   Search by literal text match (maybe regex one day?), topic, tag, or
  a combination thereof.
-   Posts in the `post/private` directory are private.

### What Does It Not Do?

**It does not include an editor.** There are many high-quality editors out
there, and the world doesn't need yet another one hacked together, or yet
another CMS that requires you to use the in-browser editor.

**It may not be easy to use unless you are a developer.** I made this for
myself, so I was able to make some very opinionated choices in order to
simplify development/fit my workflow. Namely:

-   Linux is the only supported operating system.
-   Command line interface only.
-   The live site is read-only. Updating content requires ops work, or CD.

If you are interested in using a static site generator, but want dynamic
features such as search, this might be for you.
