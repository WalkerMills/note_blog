defmodule NoteBlog.Mixfile do
  use Mix.Project

  def project do
    [
      app: :note_blog,
      name: "Noteblog",
      version: "0.9.0",
      elixir: "~> 1.13",
      elixirc_paths: elixirc_paths(Mix.env()),
      compilers: [:phoenix] ++ Mix.compilers(),
      start_permanent: Mix.env() == :prod,
      aliases: aliases(),
      deps: deps(),
      docs: docs()
    ]
  end

  # Configuration for the OTP application.
  #
  # Type `mix help compile.app` for more information.
  def application do
    [
      mod: {NoteBlog.Application, []},
      extra_applications: [:logger, :runtime_tools]
    ]
  end

  # Specifies which paths to compile per environment.
  defp elixirc_paths(:test), do: ["lib", "test/support"]
  defp elixirc_paths(_), do: ["lib"]

  # Specifies your project dependencies.
  #
  # Type `mix help deps` for examples and options.
  defp deps do
    [
      {:bimap, "~> 1.0"},
      {:cowboy, "~> 2.5"},
      {:credo, "~> 1.0", only: :dev, runtime: false},
      {:csv, "~> 2.4"},
      {:esbuild, "~> 0.4", runtime: Mix.env() == :dev},
      {:ex_doc, "~> 0.19", only: :dev, runtime: false},
      {:file_system, "~> 0.2", only: :dev},
      {:floki, "~> 0.20"},
      {:gettext, "~> 0.11"},
      {:jason, "~> 1.1"},
      {:phoenix, "~> 1.6"},
      {:phoenix_html, "~> 3.1"},
      {:phoenix_live_reload, "~> 1.2", only: :dev},
      {:phoenix_live_view, "~> 0.17.5"},
      {:phoenix_pandoc, "~> 1.1.1"},
      {:phoenix_pubsub, "~> 2.0"},
      {:plug, "~> 1.7"},
      {:plug_cowboy, "~> 2.0"},
      {:telemetry_metrics, "~> 0.6"},
      {:telemetry_poller, "~> 1.0"},
      {:timex, "~> 3.6.1"},
      {:typed_struct, "~> 0.1.3"}
    ]
  end

  defp docs do
    [
      extras: ["README.md"],
      main: "readme",
      output: "priv/static/docs"
    ]
  end

  # Aliases are shortcuts or tasks specific to the current project.
  # For example, to install project dependencies and perform other setup tasks, run:
  #
  #     $ mix setup
  #
  # See the documentation for `Mix` for more info on aliases.
  defp aliases do
    [
      setup: ["deps.get"],
      "assets.deploy": ["esbuild default --minify", "phx.digest"]
    ]
  end
end
